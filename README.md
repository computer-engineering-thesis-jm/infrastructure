# Infrastructure

Project with deployment configuration

## Configure Grafana

#### Open browser
- Go to localhost:3000

#### Sign In
Credentials
-  user = admin 
- password = secret

#### Attach prometheus datasource
- Add new datasource
- Select Prometheus
- URL: http://localhost:9090
- Access: Browser
- Http Method: GET